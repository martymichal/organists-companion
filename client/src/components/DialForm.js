import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import BookIcon from '@material-ui/icons/Book';
import NotesIcon from '@material-ui/icons/Notes';
import DeleteIcon from '@material-ui/icons/Delete';
import BackspaceIcon from '@material-ui/icons/Backspace';

const styles = theme => ({
  card: {
    height: "fit-content",
    [theme.breakpoints.up('xs')]: {
      width: "100%"
    },
    [theme.breakpoints.up('sm')]: {
      width: "420"
    },
    [theme.breakpoints.up('md')]: {
      width: 460
    },
    [theme.breakpoints.up('lg')]: {
      width: 500
    },
    [theme.breakpoints.up('xl')]: {
      width: 540
    },
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    fontSize: 40,
    height: 50,
    border: "1px solid #000"
  },
  button: {
    minWidth: 20,
    width: "100%",
    height: "100%",
    [theme.breakpoints.up('xs')]: {
      fontSize: '30px'
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '36px'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '42px'
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '46px'
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: '50px'
    },
  },
  verseButton: {
    minWidth: "30px",
    width: "100%",
    height: "100%",
    [theme.breakpoints.up('xs')]: {
      fontSize: '22px'
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '24px'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '28px'
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '30px'
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: '32px'
    },
  },
  buttonwrap: {
    marginTop: 15
  },
  root: {
    flexGrow: 1,
  },
  icon: {
    fontSize: 80
  },
  subtitle1: {
    color: "grey"
  },
  subtitle2: {
    color: "black"
  }
});

class DialForm extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      spacing: 1,
      xs: 4,
      button: {
        number: [1,2,3,4,5,6,7,8,9],
        xs: 4,
      },
      verseButton: {
        spacing: 2,
        xs: 3,
      },
    }
  };

  renderVerseButtons() {
    const { classes, data, onClick } = this.props;

    if (Array.from(data.verse).length === 0) {
      return null;
    }
    else {
      return(
        <Grid container justifyContent='center' spacing={this.state.verseButton.spacing} className={classes.buttonwrap}>
          {Array.from(data.verse).map((data) => (
            <Grid key={data} item xs={this.state.verseButton.xs}>
              <Button disableRipple variant="contained" className={classes.verseButton} color={data[1] ? "secondary" : "primary"} id="verse" value={data[0]} onClick={onClick}>
              {data[0]}
              </Button>
            </Grid>
          )) }
        </Grid>
      )
    }
  }

  renderButtons() {
    const { classes, data, onClick } = this.props;

    return (
      <Grid container justifyContent='center' spacing={this.state.spacing} className={classes.buttonwrap}>
      {this.state.button.number.map((num) => (
        <Grid key={num} item xs={this.state.button.xs}>
          <Button disableRipple variant="contained" className={classes.button} id="number" value={num} color="primary" onClick={onClick}>
          {num}
          </Button>
        </Grid>
      ))}

        <Grid item xs={this.state.button.xs}>
          <Button disableRipple variant="contained" className={classes.button} id="init_song" value="song" color={data.msgType == "song" ? ("default") : ("secondary")} onClick={onClick}>
            <BookIcon />
          </Button>
        </Grid>
        <Grid item xs={this.state.button.xs}>
          <Button disableRipple variant="contained" className={classes.button} id="number" value="0" color="primary" onClick={onClick}>
          0
          </Button>
        </Grid>
        <Grid item xs={this.state.button.xs}>
          <Button disableRipple variant="contained" className={classes.button} id="init_psalm" value="psalm" color={data.msgType == "psalm" ? ("default") : ("secondary")} onClick={onClick}>
            <NotesIcon />
          </Button>
        </Grid>
      </Grid>
    );
  }

  render() {
    const { classes, onClick, onSubmit, message, data } = this.props;

    return (
      <Card className={classes.card}>
        <CardContent>
          <form className={classes.form} onSubmit={onSubmit}>
            <div className={classes.root}>
              <Grid container justifyContent="center" alignItems='center' spacing={this.state.spacing} >
                <Grid item xs={this.state.xs}>
                  <Button disableRipple variant="contained" color="secondary" className={classes.button} id="delete" onClick={onClick}>
                    <DeleteIcon />
                  </Button>
                </Grid>
                <Grid item xs={this.state.xs}>
                  {(((data.type == "psalm" || data.type == "song") && data.msgNumber == "") ?
                    <Typography className={ classes.subtitle1 } variant="h4">
                      {data.number}
                    </Typography>
                    :
                    <Typography className={ classes.subtitle2 } variant="h3">
                      {data.msgNumber}
                    </Typography>
                  )}
                </Grid>
                <Grid item xs={this.state.xs}>
                  <Button disableRipple disableRipple variant="contained" className={classes.button} id="backspace" onClick={onClick}>
                    <BackspaceIcon />
                  </Button>
                </Grid>
              </Grid>
                {this.renderButtons()}
            </div>
          </form>
          {(data.type == "song" ? this.renderVerseButtons() : null)}
        </CardContent>
      </Card>
    )
  };
};

export default withStyles(styles)(DialForm);
