import { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import DialForm from '../components/DialForm';
import { useAuth } from '../services/AuthService';
import {useConnection } from '../services/ConnectionService.js';

function DialerPage() {
  const {user, login, quicklogin, check, refresh} = useAuth();
  const [data, setData] = useState({
    type: "",
    number: "",
    verse: [],
    activeverse: [],
    psalmtext: "",
    msgNumber: "",
    msgType: null,
  });
  const [connectionState, setConnectionState] = useState({ didRefresh: false });
  const navigate = useNavigate();

  const onMessage = (event) => {
    const msg = JSON.parse(event.data);

    // We got a message, so the connection is up -> refresh is viable again.
    setConnectionState({ didRefresh: false });

    if (msg.type === "song") {
      setData({
        ...data,
        type: msg.type,
        number: msg.number,
        verse: msg.verse,
        activeverse: msg.activeverse,
      });
    }

    if (msg.type === "psalm") {
      setData({
        ...data,
        type: msg.type,
        number: msg.number,
        psalmtext: msg.psalmtext
      });
    }

    if (msg.type === "addverse" || msg.type === "delverse") {
      setData({
        ...data,
        verse: msg.verse,
        activeverse: msg.activeverse,
      });
    }

    if (msg.type === "blank") {
      setData({
        ...data,
        type: null,
        number: null,
        verse: null,
        activeverse: null,
        psalmtext: null,
      });
    }
  };

  const onClose = (event) => {
    if (connectionState.didRefresh)
      return navigate("/login");

    if (!check())
      return navigate("/login");

    refresh();
    setConnectionState({ didRefresh: true });
  }

  const onError = (event) => {
    console.error(event);
  }

  const [connect, close, sendMessage] = useConnection(onMessage, onClose, onError);

  useEffect(() => {
    connect(user);

    return () => {
      close();
    }
  }, []);

  const handleClick = (event) => {
    event.preventDefault();

    switch (event.currentTarget.id) {
      case 'number':
        if (!data.msgType)
          return;

        if (data.msgNumber.length < 3) {
          let newNumber = event.currentTarget.value;

          setData({
            ...data,
            msgNumber: data.msgNumber.concat([newNumber]),
          });
        }
        break;

      case 'backspace':
        if (data && data.msgNumber) {
          setData({
            ...data,
            msgNumber: data.msgNumber.slice(0, -1),
          });
        }
        break;

      case 'delete':
        sendMessage({ type: "blank" });
        break;

      case 'init_song':
        if (!data.msgType) {
          setData({
            ...data,
            msgType: event.currentTarget.value,
          });
        } else if (data.msgType === "song" && data.msgNumber) {
          const msg = {
            type: data.msgType,
            number: data.msgNumber,
          }

          sendMessage(msg);

          setData({
            ...data,
            msgNumber: "",
            msgType: null,
          });
        }
        break;

      case 'init_psalm':
        if (!data.msgType) {
          setData({
            ...data,
            msgType: event.currentTarget.value,
          });
        } else if (data.msgType === "psalm" && data.msgNumber) {
          const msg = {
            type: data.msgType,
            number: data.msgNumber,
          }

          sendMessage(msg);

          setData({
            ...data,
            msgNumber: "",
            msgType: null,
          });
        }
        break;

      case 'verse':
        let verse = parseInt(event.currentTarget.value);
        let msg = {
          verse: verse,
        };

        if (!data.activeverse || data.activeverse.indexOf(verse) == -1)
          msg.type = 'addverse';
        else
          msg.type = 'delverse';

        sendMessage(msg);
        break;
    }
  };

  return (
    <div className='page-parent'>
      <Grid container justifyContent='center' alignItems='center' direction='column' spacing={8} className='container-full'>
        <Grid item className='container-dialer'>
          <DialForm data={data} onClick={handleClick} onSubmit={sendMessage} />
        </Grid>
      </Grid>
    </div>
  )
}

export default DialerPage;
