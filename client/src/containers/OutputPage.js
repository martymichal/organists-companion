import { Fragment, useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

import { useAuth } from '../services/AuthService.js';
import { useConnection } from '../services/ConnectionService.js';

const useStyles = makeStyles(theme => ({
  h4Song: {
    lineHeight: '0.85em',
    fontWeight: '400',
    margin: 0,
    padding: 0,
    [theme.breakpoints.up('xs')]: {
      fontSize: '12rem'
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '15rem'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '30rem'
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '36rem'
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: '46rem'
    },
  },
  h4Song_small: {
    lineHeight: '0.95em',
    fontWeight: '400',
    margin: 0,
    padding: 0,
    [theme.breakpoints.up('xs')]: {
      fontSize: '12rem'
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '15rem'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '22rem'
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '24rem'
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: '32rem'
    }
  },
  h4Psalm: {
    [theme.breakpoints.up('xs')]: {
      fontSize: '2.6rem'
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '3.5rem'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '6.5rem'
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '7.8rem'
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: '10rem'
    },
  },
  display3Verse: {
    [theme.breakpoints.up('xs')]: {
      fontSize: '1.8rem'
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '2.5rem'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '5rem'
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '7rem'
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: '11rem'
    },
  }
}));

function Song({ data }) {
  const classes = useStyles();

  return (
    <Grid container className='container-main' direction='column' justifyContent='center' alignItems='center'>
      <Grid item >
        <Typography className={ data.verse.length > 6 ? classes.h4Song_small : classes.h4Song } variant='h4' color='inherit'>
          {data.number}
        </Typography>
      </Grid>
      {data.verse.length !== 0 ?
        <Grid item container alignItems='center' justifyContent='center' >
          <Grid item xs />
          <Grid item container justifyContent='center' alignItems='center' xs={10}>
            {Array.from(data.verse).map((number) => (
              <Grid key={number} item xs={2} >
                <Typography className={classes.display3Verse} variant='h2' color='inherit'>{number}</Typography>
              </Grid>
            ))}
          </Grid>
          <Grid item xs />
        </Grid>
      : Fragment}
    </Grid>
  );
}

function Psalm({ data }) {
  const classes = useStyles();

  return (
    <Grid container className='container-main' justifyContent='center' alignItems='center'>
      <Grid item className='wrap-main'>
        <Typography className={classes.h4Psalm} variant='subtitle1'>Žalm: {data.number}</Typography>
        <Typography className={classes.h4Psalm} variant='h4' color='inherit'>
          {data.psalmtext}
        </Typography>
      </Grid>
    </Grid>
  )
}

function OutputPage() {
  const { user, login } = useAuth();
  const [data, setData] = useState(null);

  const onMessage = (event) => {
    const msg = JSON.parse(event.data);

    if (msg.type === "song") {
      setData({
        ...data,
        type: msg.type,
        number: msg.number,
        verse: msg.activeverse,
      });
    }

    if (msg.type === "psalm") {
      setData({
        ...data,
        type: msg.type,
        number: msg.number,
        psalmtext: msg.psalmtext
      });
    }

    if (msg.type === "addverse" || msg.type === "delverse") {
      setData({
        ...data,
        verse: msg.activeverse,
      });
    }

    if (msg.type === "blank") {
      setData({
        ...data,
        type: null,
        number: null,
        psalmtext: null,
      });
    }
  };

  const onClose = (event) => {
    console.error(`Connection closed: Reason ${event.code}`);
    login({ username: "guest" });
  }

  const onError = (event) => {
    console.error(`Connection error: ${JSON.stringify(event)}`);
    login({ username: "guest" });
  };

  const [connect, close, sendMessage] = useConnection(onMessage, onClose, onError);

  useEffect(() => {
    connect(user);

    return () => {
      close();
    };
  }, [user]);

  useEffect(() => {
    login({ username: "guest" });

    return () => {
      close();
    };
  }, []);

  let content = null;
  if (data && data.type === "song")
    content = <Song data={data} />;
  else if (data && data.type === "psalm")
    content = <Psalm data={data} />;

  return (
    <div className='output-parent'>
      {content}
    </div>
  )
}

export default OutputPage;
