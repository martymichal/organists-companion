const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const debug = require('debug')('api');

const config = require('../config/config');
const auth = require('../utils/auth');
const database = require('../utils/database');
const tokens = require('../utils/tokens');

/* User Routes */

router.post('/user/login', async function (req, res, next) {
  console.debug("API /user/login: Enter");

  let user = null;
  try {
    user = await auth.login(req);
  } catch (err) {
    console.error(err);

    if (err === "Incorrect username" || err === "Incorrect password")
      return res.status(401).json({ success: false, wrongCredentials: true });

    return res.status(500).json({ success: false, message: err });
  }

  const tokenPayload = {
    username: user.username,
    permissionLevel: user.permissionLevel
  }

  const newTokens = { accessToken: null, refreshToken: null };
  try {
    newTokens.accessToken = await tokens.generateAccessToken(tokenPayload);
    newTokens.refreshToken = await tokens.generateRefreshToken(tokenPayload);
  } catch (err) {
    console.error(err);

    if (err === "No changes durig token update" || err === "No token was inserted" || err === "No token provided" || err === "") {
      return res.status(400).json({
        error: true,
        message: "There was an error on the server."
      })
    }

    if (err === "Unidentified error during login" || err) {
      return res.status(400).json({
        error: true,
        message: "There was an error on the server."
      })
    }
  }

  console.debug(`AT: ${newTokens.accessToken} ; RT: ${newTokens.refreshToken}`);

  if (newTokens.accessToken && newTokens.refreshToken) {
    await tokens.setRefreshToken(newTokens.refreshToken);

    return res.status(200).json({
      success: true,
      accessToken: newTokens.accessToken,
      refreshToken: newTokens.refreshToken.token
    })
  }

  return res.status(500).json({ success: false, message: "Unknown error" });
});

router.post('/user/quicklogin', async function (req, res) {
  console.debug("API /user/quicklogin: Enter");

  let user = null;
  try {
    user = await auth.quickLogin(req);
  } catch (err) {
    console.error(err);

    if (err === "Incorrect username" || err === "Incorrect password") {
      return res.status(401).json({
        success: false,
        wrongCredentials: true
      });
    }

    return res.status(500).json({ success: false, message: err })
  }

  const tokenPayload = {
    username: user.username,
    permissionLevel: user.permisionLevel
  }

  const newTokens = { accessToken: null, refreshToken: null };
  try {
    newTokens.accessToken = await tokens.generateAccessToken(tokenPayload);
    newTokens.refreshToken = await tokens.generateRefreshToken(tokenPayload);
  } catch (err) {
    console.error(err);

    if (err === "No token" || "Invalid token") {
      return res.status(401).json({
        success: false,
        wrongToken: true
      });
    }

    return res.status(500).json({ success: false, message: err })
  }

  const oldRefreshToken = req.body.refreshToken;

  // Replace old refreshToken with a new one
  tokens.setRefreshToken(newTokens.refreshToken, oldRefreshToken);

  return res.status(200).json({
    success: true,
    accessToken: newTokens.accessToken,
    refreshToken: newTokens.refreshToken.token
  })
});

router.post('/user/verify', async function (req, res) {
  console.debug("API /user/verify: Enter");

  const accessToken = req.body.accessToken;

  console.debug(`AT: ${accessToken}`);

  if (!accessToken)
    return res.status(401).json({ success: false });

  let decodedToken = null;
  try {
    decodedToken = await tokens.verifyAccessToken(accessToken);
  } catch (err) {
    console.error(err);

    if (err === "Invalid active token.")
      return res.status(401).json({ success: false, message: err });
    else
      return res.status(500).json({ success: false, message: "There was an error on the server." });
  }

  if (decodedToken)
    return res.status(200).json({ success: true });

  return res.status(500).json({ success: false, message: "There was an unhandled situation" });
})

/* Tokens Routes */

router.post('/tokens/refresh', async function(req, res) {
  console.debug("API /tokens/refresh: Enter");

  const refreshToken = req.body.refreshToken;

  console.debug(`Old refresh token ${refreshToken}`);

  if (!refreshToken)
    return res.status(401).json({ success: false, message: "Refresh token was not provided" });

  let isRefreshFine = false;
  try {
    isRefreshFine = await tokens.verifyRefreshToken(refreshToken);
  } catch (err) {
    console.error(`API /tokens/refresh: ${err}`);

    if (err === "Refresh token was not provided")
      return res.status(401).json({ success: false, message: err });

    if (err === "Refresh token is not valid")
      return res.status(401).json({ success: false, message: err });

    if (err === "Refresh token is not whitelisted")
      return res.status(401).json({ success: false, message: err });
  }

  if (!isRefreshFine)
    return res.status(401).json({ success: false, message: "Refresh token is not valid" });

  const sql = "SELECT username FROM tokens WHERE token = ?";
  const rows = await database.selectOne("username", "tokens", "token", [refreshToken]);

  const newPayload = {
    username: rows.username,
    permission: rows.permissionLevel
  }

  let refreshTokens = null;
  try {
    refreshedTokens = await tokens.refresh(refreshToken, newPayload);
  } catch (err) {
    console.error(`API /tokens/refresh: {err}`);

    if (err === "Error during token generation")
      return res.status(500).json({ success: false, message: err });

    return res.status(401).json({ success: false, message: "There was an error during token refresh." });
  }

  console.debug(`New refresh token: ${refreshedTokens.refreshToken}`);

  if (refreshedTokens)
    return res.status(200).json({ success: true, accessToken: refreshedTokens.accessToken, refreshToken: refreshedTokens.refreshToken });

  return res.status(500).json({ success: false, message: "There was an unhandled situation" });
})

/* Psalms Routes */

router.get('/psalms', async function(req, res) {
  try {
    let query = req.query;
    let psalms = await database.selectAll("id, text", "psalms", "id");

    if (psalms) {
      if (!query.from && !query.to) {
        return res.status(200).json({ success: true, psalms: psalms })
      } else {
        query.from--;

        if (!query.to)
          query.to = psalms.length;

        psalms = psalms.slice(query.from, query.to);

        return res.status(200).json({ success: true, psalms: psalms });
      }
    }
  } catch (e) {
    console.error(e);

    return res.status(400).json({ success: false });
  }
})

module.exports = router;
