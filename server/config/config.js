const defaultSecretString = 't0ps3cur3s3cr3t';

let secretString = process.env.COMPANION_SECRET;
if (!secretString || secretString.length == 0) {
  secretString = defaultSecretString;
}

const secret = Buffer.from(secretString).toString('hex');

module.exports = {
  'token': {
    'secret': secret,
    'expiresIn': '480m'
  }
};
