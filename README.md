# Organist's Companion

Organist's Companion is meant to be used with RaspberryPi or other mini computer to replace old analogue systems in christian churches used for showing people during masses what song/psalm is sang.

The server runs on Node.js with Express web framework and uses SQLite database. The client is made using React with Material-UI library.

Technologies used:
- Passport
- JsonWebTokens
- WebSockets

This is my very first project ever.


## Available scripts

`npm run start`

A basic script that only starts the server

`npm run dev`

This starts both node server and react client.
Server runs on port `3001` and client on port `3000`.

`npm run build`

This script installs all needed packages in the project and then builds the react client.

`npm run production-mini`

This starts already production-ready project.

`npm run production-full`

This prepares the whole project for production (installs dependencies), builds the react client and then launches the app


These scripts are **very bad** and I will change them in the future to simplify both development and production workflows.


## Deployment

### Prerequisities

- Raspberry Pi or some other computer
- TV/computer screen to output song numbers/texts
- local network (wired/wireless depending on environment)
- controller (optional) -> a mobile phone is just enough

### Process

- install Node on your Raspberry
- download the app (maybe in the future packaged version)
- install the app `npm install`
- improvise - use Google Chrome, Mozilla Firefox or other browser with kiosk mode and try to set it up
- be angry at me for creating this :) (**BONUS**)


## License

MIT